var db = firebase.firestore();


const topp = 0;
const parent = i => ((i + 1) >>> 1) - 1;
const left = i => (i << 1) + 1;
const right = i => (i + 1) << 1;

class PriorityQueue {
  constructor() {
    this._heap = [];
    this._comparator = (a, b) =>{
      if(a[1]===b[1]){
        return a[0]>b[0]
      }
      return a[1]>b[1]
     }
  }
  show(){
    return  this._heap;
  }
  size() {
    return this._heap.length;
  }
  isEmpty() {
    return this.size() == 0;
  }
  peek() {
    return this._heap[topp];
  }
  push(...values) {
    values.forEach(value => {
      this._heap.push(value);
      this._siftUp();
    });
    return this.size();
  }
  pop() {
    const poppedValue = this.peek();
    const bottom = this.size() - 1;
    if (bottom > topp) {
      this._swap(topp, bottom);
    }
    this._heap.pop();
    this._siftDown();
    return poppedValue;
  }
  replace(value) {
    const replacedValue = this.peek();
    this._heap[topp] = value;
    this._siftDown();
    return replacedValue;
  }
  _greater(i, j) {
    return this._comparator(this._heap[i], this._heap[j]);
  }
  _swap(i, j) {
    [this._heap[i], this._heap[j]] = [this._heap[j], this._heap[i]];
  }
  _siftUp() {
    let node = this.size() - 1;
    while (node > topp && this._greater(node, parent(node))) {
      this._swap(node, parent(node));
      node = parent(node);
    }
  }
  _siftDown() {
    let node = topp;
    while (
      (left(node) < this.size() && this._greater(left(node), node)) ||
      (right(node) < this.size() && this._greater(right(node), node))
    ) {
      let maxChild = (right(node) < this.size() && this._greater(right(node), left(node))) ? right(node) : left(node);
      this._swap(node, maxChild);
      node = maxChild;
    }
  }
}

const queue = new PriorityQueue((a, b) => a[1] > b[1]);
// queue.push(['low', 0], ['medium', 5], ['high', 10]);
// console.log('\nContents:');
// while (!pairwiseQueue.isEmpty()) {
//   console.log(pairwiseQueue.pop()[0]); //=> 'high', 'medium', 'low'
// }
let btn=document.getElementById("invoke");
btn.addEventListener("click", invoke);
function invoke(){
    let choice=document.getElementById("choice").value;
    switch(choice){
        case "1":           
            let value = +prompt("Enter value: ");
            let priority = +prompt("Enter priority: ");
            queue.push([value,priority]);
            alert("element was added");
            break;
        case "2":
            let popped= queue.pop();
            alert(`${popped} was removed`);           
            break;
        case "3":
            alert(queue.peek());
            break;
        case "4":
            alert(queue.size());
            break;
        case "5":
            let display = queue.show(); 
            alert(display.join("\n"));
            break;
        case "6":
            let temp = queue.show();

            var length=Object.keys(temp).length;
            console.log(length);
            var b = {};
            var i =0;
            temp.forEach(x=>{
               b[i]=x.join("-");
               i++ 
            });
            console.log(b);
            db.collection("Quque").doc("PriorityQueue").update(b)
            .then(function() {
            window.alert("Document successfully updated!");        
            })
            .catch(function(error) {
            window.alert("Incorrect ID.");       
             });
          
            break;
        case "7":
            var obj = {};
            db.collection("Quque").doc("PriorityQueue").get()
            .then(function(doc) {
                if (doc.exists) {
                    obj=doc.data();
                    console.log("Document data:", doc.data());
                     window.alert("retryiving was successfull!");
                     console.log(obj);
                     while (!queue.isEmpty()) {
                       queue.pop();
                     }
                     var length2=Object.keys(obj).length;
                     for(var i = 0; i < length2;i++){
                         var index =obj[i].indexOf("-");    
                         var valueTemp = obj[i].slice(0,index);
                         var priorityTemp = obj[i].slice(index+1);
                         queue.push([valueTemp,priorityTemp]);
                     }
                } else {
                    // doc.data() will be undefined in this case
                    console.log("No such document!");
                }
            }).catch(function(error) {
                console.log("Error getting document:", error);
            });


            break;
        case "0":
            break ;
        default:
            alert("Incorrect input!");
            break;
    }



}/*
out: while(true){
    let choice=prompt(` 1. Add data to quque
    2. Remove from the quque
    3. Head of the quque
    4. Size of the quque
    5. Print the quque
    6. send to DB
    7. Retrieve data from DB
    0. Exit`);
    switch(choice){
        case "1":           
            let value = +prompt("Enter value: ");
            let priority = +prompt("Enter priority: ");
            queue.push([value,priority]);
            alert("element was added");
            break;
        case "2":
            let popped= queue.pop();
            alert(`${popped} was removed`);           
            break;
        case "3":
            alert(queue.peek());
            break;
        case "4":
            alert(queue.size());
            break;
        case "5":
            let display = queue.show(); 
            alert(display.join("\n"));
            break;
        case "6":
            let temp = queue.show();

            var length=Object.keys(temp).length;
            console.log(length);
            var b = {};
            var i =0;
            temp.forEach(x=>{
               b[i]=x.join("-");
               i++ 
            });
            console.log(b);
            db.collection("Quque").doc("PriorityQueue").update(b)
            .then(function() {
            window.alert("Document successfully updated!");        
            })
            .catch(function(error) {
            window.alert("Incorrect ID.");       
             });
          
            break;
        case "7":
            var obj = {};
            db.collection("Quque").doc("PriorityQueue").get()
            .then(function(doc) {
                if (doc.exists) {
                    obj=doc.data();
                    console.log("Document data:", doc.data());
                } else {
                    // doc.data() will be undefined in this case
                    console.log("No such document!");
                }
            }).catch(function(error) {
                console.log("Error getting document:", error);
            });
            while (!queue.isEmpty()) {
              queue.pop();
            }
            var length2=Object.keys(obj).length;
            for(var i = 0; i < length2;i++){
                var index =obj[i].indexOf("-");    
                var valueeTemp = obj[i].slice(0,index);
                var priorityTemp = obj[i].slice(index+1);
                queue.push([valueTemp,priorityTemp]);
            }

            break;
        case "0":
            break out;
    }
}    
*/