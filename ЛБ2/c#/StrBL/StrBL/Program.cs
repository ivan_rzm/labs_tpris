﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;


namespace StrBL
{
    class Program
    {
        static void Main(string[] args)
        {//D:/Texts/TPRIS/text.txt
        
        String text = "",
                path = "D:\\Texts\\TRIS\\text.txt";
        int n = 3;
        Text t = new Text(text);        
        t.readText(path);    
        String str = t.StrBl(t.getText(), n);        
       Console.WriteLine("Text: \n" + t.getText());
       Console.WriteLine("Changed text: \n" + str);
        
        t.writeText("D:\\Texts\\TPRIS\\rs.txt", str);
        Console.ReadLine();
        }
    }
    class Text
    {
        private String text;

        public Text(String text)
        {
            this.text = text;
        }

        public String getText()
        {
            return text;
        }

        public int getSize()
        {
            return text.Length;
        }

        public String StrBl(String str, int n)
        {
            
            String res = str.Substring(0, n) + str.Substring(n).ToLower();

            return res;
        }

        public void readText(String path)
        {
            try
            {
                String t = File.ReadAllText(path); 
                this.text = t;
            }
            catch (FileNotFoundException ex)
            {
                
                Console.WriteLine("Incorrect file path!");
            }
            catch (IOException exc)
            {   
                Console.WriteLine(exc.Message);
                
            }
        }

        public void writeText(String path, String text)
        {
            Exception ex = checkExceptions(text);
            if (ex != null)
            {
                Console.WriteLine(ex.Message);
                return;
            }

            try
            {
                StreamWriter fw = new StreamWriter(path);
                fw.Write(text);

               Console.WriteLine("Text's written.");

                fw.Close();
            }
            catch (IOException e)
            {
                Console.WriteLine(e.Message);
            }
        }

        public Exception checkExceptions(String text)
        {
            if (text == null || "".Equals(text))
                return new Exception("The string is empty");
            else if (text.Length >= 1e+8)
                return new Exception("The string is too large");

            return null;
        }
    }
}
