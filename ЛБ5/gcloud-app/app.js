var express = require("express");
var admin =require('firebase-admin');
var serviceAccount = require('./tpris3-58cd5-firebase-adminsdk-slty4-3cae20370f.json');
var bodyParser = require("body-parser");

 

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://tpris3-58cd5.firebaseio.com"
});

var db=admin.firestore();


var app = express();
var jsonParser = bodyParser.json();
 
app.use(express.static(__dirname + "/public"));



app.get("/api/getlist", function(req, res){  
    db.collection("lab5").doc("list").get()
    .then(function(doc) {
        if (doc.exists) {
         
            console.log("Document data:", doc.data());
            var data = JSON.stringify(doc.data());

          
            res.send(data);

    }}).catch(function(error) {
        console.log("Error getting document:", error);
    });

});

app.post("api/postlist", jsonParser, function (req, res) {
     
    if(!req.body) return res.sendStatus(400);
    console.log(req.body);
    db.collection("lab5").doc("list").update(req.body)
    .then(function() {
    console.log("Document successfully updated!");        
    })
    .catch(function(error) {
    console.log("Incorrect ID.");       
     });
    res.send(req.body);
});

app.get("/api/gettree", function(req, res){ 

    db.collection("lab5").doc("tree").get()
    .then(function(doc) {
        if (doc.exists) {
         
            console.log("Document data:", doc.data());
            var data = JSON.stringify(doc.data());

            // отправляем удаленного пользователя
            res.send(data);

    }}).catch(function(error) {
        console.log("Error getting document:", error);
    });


});

app.post("/api/posttree", jsonParser, function (req, res) {

    if(!req.body) return res.sendStatus(400);
    console.log(req.body);
    db.collection("lab5").doc("tree").update(req.body)
    .then(function() {
    console.log("Document successfully updated!");        
    })
    .catch(function(error) {
    console.log("Incorrect ID.");       
     });
    res.send(req.body);

});



app.listen(8080, function(){
    console.log("server is waiting for connections...");
});