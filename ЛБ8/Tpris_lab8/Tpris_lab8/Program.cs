﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Tpris_lab8
{
    class Program
    {
        private int V;   // No. of vertices
        private LinkedList<int>[] adj; //Adjacency List
        private int[,] matrix;
        private bool[] visited;

        Program(int v)
        {
            V = v;
            adj = new LinkedList<int>[v];
            for (int i = 0; i < v; ++i)
                adj[i] = new LinkedList<int>();
            matrix = new int[V, V];
            visited = new bool[V];
            for (int i = 0; i < V; i++)
                visited[i] = false;

        }

        // Function to add an edge into the graph
        void addEdge(int v, int w)
        {
            //LinkedListNode<int> a = new LinkedListNode<int>(v);
            //  LinkedListNode<int> b = new LinkedListNode<int>(w);
            adj[v].AddLast(w);
            adj[w].AddLast(v);


            matrix[v, w] = 1;
            matrix[w, v] = 1;
        }
        // Function to add an edge into the graph

        void printMatrix()
        {
            //Console.WriteLine("adjacency matrix");
            for (int i = 0; i < V; i++)
            {
                for (int j = 0; j < V; j++)
                {
                    Console.Write(matrix[i, j] + " ");
                }
                Console.WriteLine();
            }

        }

        public void fillMatrix()
        {

            Random rand = new Random();

            for (int i = 0; i < V; i++)
            {
                for (int j = i + 1; j < V; j++)
                {
                    matrix[i, j] = rand.Next(2);
                    matrix[j, i] = matrix[i, j];
                    if (matrix[i, j] == 1)
                    {
                        adj[i].AddLast(j);
                    }


                }
            }

        }


        
        Boolean isCyclicUtil(int v, int parent)
        {
            
            visited[v] = true;


            LinkedListNode<int> i = adj[v].First;
            
            foreach (int element in adj[v])
            {
                
                if (visited[element] == false)
                {
                    if (isCyclicUtil(element, v) == true)
                    {

                        return true;
                    }
                }

                            else if (element != parent)
                {

                    return true;
                }
                i = i.Next;

            }
            return false;
        }

        
        Boolean isTree()
        {



            Task<bool> task1 = new Task<bool>(() =>
            {
                if (isCyclicUtil(0, -1) == true)
                    return false;
                else
                {
                    return true;
                }
            });
            task1.Start();
            task1.Wait();
            bool task1Result = task1.Result;
            if (!task1Result)
                return false;


            Task<bool> task2 = new Task<bool>(() =>
            {
                for (int u = 0; u < V; u++)
                    if (!visited[u])
                        return false;
                return true;
            });
            task2.Start();
            task2.Wait();
            bool task2Result = task2.Result;
            if (!task2Result)
                return false;
            return true;
        }





        static void Main(string[] args)
        {
            Program g1 = new Program(5);
            //g1.fillMatrix();

            g1.addEdge(1, 0);
            g1.addEdge(0, 2);
            g1.addEdge(0, 3);
            g1.addEdge(3, 4);

            Console.WriteLine("first matrix");
            g1.printMatrix();

            if (g1.isTree()) { Console.WriteLine("Graph is Tree"); }
            else { Console.WriteLine("Graph is not Tree"); }

            Console.WriteLine("second matrix");
            Program g2 = new Program(5);

            g2.addEdge(1, 0);
            g2.addEdge(0, 2);
            g2.addEdge(2, 1);
            g2.addEdge(0, 3);
            g2.addEdge(3, 4);

            g2.printMatrix();
            if (g2.isTree())
            {
                Console.WriteLine("Graph is Tree");
            }
            else { Console.WriteLine("Graph is not Tree"); }
            Console.ReadLine();





        }
    }
}
