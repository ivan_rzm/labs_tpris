var db = firebase.firestore();
const updBtn = document.getElementById("update-info");
updBtn.addEventListener('submit', updateInfo);
const addBtn = document.getElementById("add-info");
addBtn.addEventListener('submit', addInfo);
const delBtn = document.getElementById("delete-info");
delBtn.addEventListener('submit', deleteInfo); 
const searchBtn = document.getElementById("search");
searchBtn.addEventListener('submit', search);
const cmpBtn = document.getElementById("compare-info");
cmpBtn.addEventListener('submit', compare); 
const displayMenu = document.querySelector("#nav-menu details:nth-of-type(6) > summary");
displayMenu.addEventListener('click', display);
const updSelect = document.getElementById("update-info-field");
updSelect.addEventListener("change", updateFeildChanged);
const searchSelect = document.getElementById("search-info-field");
searchSelect.addEventListener("change", searchFieldChanged);




setInfo = function(docRef, obj, str){
    docRef.set(obj).then(e => {
        window.alert(str);
        $("#add-data")[0].reset();
    })
    .catch(error => console.error("Error: ", error));
}


addInfo = function(){
    const id = document.querySelector("#kind").value;  
    var obj = getInputedData();    
    db.collection("RedBook").doc(id).get().then( (doc) =>{
        if(doc && doc.exists){
            var update = window.confirm("The document already exist. UPDATE it?");
            if(update){
                setInfo(db.collection("RedBook").doc(id), obj, "Information was updated!");
            }
        } else{
            addInfoInDb();  
        }
    });
}

searchFieldChanged = function(){
    const selected = document.getElementById("search-info-field").value;
    const node = document.getElementById("search-data-option");        
    if(selected === "number" || selected === "population"){
        node.childNodes[1].style.display = "none";
        node.childNodes[3].style.display = "none";
        node.childNodes[5].style.display = "none";
        node.childNodes[9].style.display = "block";
        node.childNodes[13].style.display = "block";
        node.childNodes[7].selected = true;
    } else{
        node.childNodes[1].style.display = "block";
        node.childNodes[3].style.display = "block";
        node.childNodes[5].style.display = "block";
        node.childNodes[9].style.display = "none";
        node.childNodes[13].style.display = "none";
        node.childNodes[1].selected = true;    
    }   
}

updateFeildChanged = function(){
    const node = document.getElementById("update-info-field");
    const value = node.options[node.selectedIndex].text;    
    const valueInput = document.querySelector(".update-info-field input[id='value']");
    const valueInputLabel = document.querySelector("label[for='upd-field']");
    valueInputLabel.innerHTML = `${value}`;
    valueInput.placeholder = `new ${value}`;
    valueInput.value = "";   
    if(value === 'number' || value === 'population'){
        valueInput.attributes["type"].value = "number";
        valueInput.min = "0";
    } else{
        valueInput.attributes["type"].value = "text";
    }
}


dataForUpdate = function(){
    const id = document.querySelector(".update-info-field input[id='id']").value;
    let value = document.querySelector(".update-info-field input[id='value']").value;
    const fieldKey = document.getElementById("update-info-field").value;
    
    if(fieldKey === 'number' || fieldKey === 'population'){
        value = +value;
    }
    
    
    var newData = {
        [fieldKey]: value
    };
    
    return newData;
}


searchForStringField = function(field,option,value){
    let arr = null;
    let count = 0;    
   db.collection("RedBook").get()
        .then( querySnapshot => {
            querySnapshot.forEach( doc => {
                const data = doc.data();
                if(option === "starts"){ 
                    if(field.includes(".")){ 
                        arr = field.split(".");
                        
                        if(data[arr[0]][arr[1]].startsWith(value)){
                            appendToTable("search-table", "search-table-body",data);                            
                            count++;                            
                        }
                    } else{ 
                        if(data[field].startsWith(value)){
                            appendToTable("search-table", "search-table-body",data);                            
                            count++;                            
                        }    
                    }    
                } else if(option === "ends"){
                    if(field.includes(".")){
                        arr = field.split(".");
                        if(data[arr[0]][arr[1]].endsWith(value)){
                            appendToTable("search-table","search-table-body", data);
                            count++;
                        }
                    } else{
                        if(data[field].endsWith(value)){
                            appendToTable("search-table", "search-table-body", data);
                            count++;
                        }    
                    }
                } else{
                     if(field.includes(".")){
                        arr = field.split(".");                        
                        if(data[arr[0]][arr[1]].includes(value)){
                            appendToTable("search-table", "search-table-body", data);                            
                            count++;
                        }
                    } else{
                        if(data[field].includes(value)){
                            appendToTable("search-table", "search-table-body", data);                            
                            count++;                            
                        }    
                    }
                }  
            });        
            if(count === 0){     
                    window.alert("No result due to the current query.");      
            }
        })
        .catch( error => {
            console.error("Error getting documents: ", error);
        });    
    return true;
}

updateInfo = function(){
    const id = document.querySelector(".update-info-field input[id='id']").value; 
    var obj = dataForUpdate();    
    db.collection("RedBook").doc(id).update(obj).then(function() {
        window.alert("Document successfully updated!");
        document.querySelector(".update-info-field input[id='value']").value = "";
    })
    .catch(function(error) {
        window.alert("Incorrect ID.");
  
    });
    
}


deleteInfo = function(){
    const id = document.querySelector("#delete-info input[type='text']").value;
    db.collection("RedBook").doc(id).get().then( (doc) =>{
        if(doc && doc.exists){
            db.collection("RedBook").doc(id).delete().then( (delDoc) => {
                window.alert("Document successfully deleted!");
            })
            .catch(error => console.error("Error removing document: ", error));
        } else{
            window.alert("Incorrect ID.");    
        }
    });          
}


validateComparison = function(value1, value2){
    resLabel = document.getElementById("cmp-res");    
    if(value1 === undefined && value1 === undefined){
        resLabel.innerHTML = "Incorrect IDs.";
        return;
    } else if(value1 === undefined){
        resLabel.innerHTML = "Incorrect first object ID.";
        return;
    } else if(value2 === undefined){
        resLabel.innerHTML = "Incorrect second object ID.";
        return;
    } else{
        compareBy(value1,value2);
    }   
}

compareBy = function(first, second){
    const key = document.getElementById("compare-info-field").value;
    const field = key;
    
    var arr = [];
    let value1 = null;
    let value2 = null;
    
    if(field.includes(".")){ 
        arr = field.split(".");
                    
        value1 = first[arr[0]][arr[1]];
        value2 = second[arr[0]][arr[1]];
    } else{
        value1 = first[field];
        value2 = second[field];    
    }
    
    if(value1 > value2){
        resLabel.innerHTML = `First object ${field} value is greater than second one.`;    
    } else if(value1 < value2) {
        resLabel.innerHTML = `Second object ${field} value is greater than first one.`;
    } else{
        resLabel.innerHTML = `Objects ${field} values are equal.`;
    }
}

compare = function(){
    const firstId = document.getElementById("first").value;
    const secondId = document.getElementById("second").value;    
    var firstDoc;
    var secondDoc;
        
    db.collection("RedBook").get().then(function(querySnapshot) {
        querySnapshot.forEach( doc => {
            if(doc.id === firstId){
                firstDoc = doc.data();
            } else if(doc.id === secondId){
                secondDoc = doc.data();
            }
        });     
        validateComparison(firstDoc, secondDoc);
    });   
}



addInfoInDb = function(){    
    const id = document.querySelector("#kind").value;    
    var obj = getInputedData();
    setInfo(db.doc(RedBook/${id}), obj, "Info was added");
}



appendToTable = function(tableId, tbody, data){
    const table = document.getElementById(tableId);
    let tableBody = document.getElementById(tbody);
    let row = document.createElement("tr");
    let cell = document.createElement("td");
    let text = document.createTextNode(data.kind);
    cell.appendChild(text);
    row.appendChild(cell);    
    cell = document.createElement("td");
    text = document.createTextNode(data.clan);
    cell.appendChild(text);
    row.appendChild(cell);    
    cell = document.createElement("td");
    text = document.createTextNode(data.family);
    cell.appendChild(text);
    row.appendChild(cell);    
    cell = document.createElement("td");
    text = document.createTextNode(data.home);
    cell.appendChild(text);
    row.appendChild(cell);    
    cell = document.createElement("td");
    text = document.createTextNode(data.number);
    cell.appendChild(text);
    row.appendChild(cell);    
    cell = document.createElement("td");
    text = document.createTextNode(data.population);
    cell.appendChild(text);
    row.appendChild(cell);
    tableBody.appendChild(row);
    table.appendChild(tableBody);
}

display = function(){
    $("tbody").children().remove();
    
    const table = document.getElementById("show-table");
    const caption = document.getElementById("show-caption");
    const showBar = document.getElementById("show-bar");
    
    const showMenu = document.querySelector("#nav-menu details:nth-of-type(6)");
    if(showMenu.value != open){
        showMenu.value = open;
    } else{
        showMenu.value = "";
    }
        
    let count = 0;
    if(showMenu.value == open){
        caption.style.display = "none";    
        table.style.display = "none"; 
            caption.style.display = "block";
            table.style.display = "table";        
        db.collection("RedBook").get().then((querySnapshot) => {
            querySnapshot.forEach((doc) => {
                if(doc && doc.exists){
                    appendToTable("show-table", "show-tbody", doc.data());
                    count++;                   
                }
            });
            if(count === 0){
                    window.alert("No result due to the current query.");  
            }
        })
        .catch(error => console.error("Error: ", error));    
    }
}




search = function(){
    $("tbody").children().remove();    
    const table = document.getElementById("search-table");
    const caption = document.getElementById("search-caption");
    const searchBar = document.getElementById("search-bar");
    caption.style.display = "none";    
    table.style.display = "none";    
    const field = document.getElementById("search-info-field").value;
    const key = field;
    const option = document.getElementById("search-data-option").value;
    let inputedValue = document.getElementById("srch-input").value;
    var isValidated;
    if(option === "contains" || option === "starts" || option === "ends"){
        isValidated = searchForStringField(key,option,inputedValue);
    } else{
        isValidated = searchForField(key,option,inputedValue);   
    }    
    
    if(isValidated){   
            caption.style.display = "block";
            table.style.display = "table";       
    }
}
getInputedData = function(){
    const id = document.querySelector("#kind").value;
    const _kind = id;
    const _clan = document.querySelector("#clan").value;
    const _family = document.querySelector("#family").value;
    const _home = document.querySelector("#home").value;
    const _number = +document.querySelector("#number").value;
    const _population = +document.querySelector("#population").value;
    
    obj = {
        kind : _kind ,
        clan : _clan ,
        family : _family ,
        home : _home ,
        number: _number,
        population: _population
        
    };    
    return obj;
}
searchForField = function(field,option,value){  
    if(field === "number" || field === "population"){
        value = +value;
    }    
    if(isNaN(value)){
        window.alert("Incorrect data type");
        return false;
    }    
    let count = 0;
    var query = db.collection("RedBook").where(field, option, value);
    query.get()
        .then( querySnapshot => {
            querySnapshot.forEach( doc => {
                const data = doc.data();                
                appendToTable("search-table", "search-table-body", data);                
                count++;  
            });
            
            if(count == 0){ 
                    window.alert("No result due to the current query.");
            }
        })
        .catch( error => {
            console.error("Error getting documents: ", error);
        });
    
    return true;
}








